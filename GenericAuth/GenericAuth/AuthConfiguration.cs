﻿namespace GenericAuth
{
    public class AuthConfiguration
    {
        public string AppName { get; set; }
        public DefaultAuthProvider DefaultAuthProvider { get; set; }
        public SessionDictionary Options { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Security.Principal;
using System.Threading;
using GenericAuth.Core.TwoFactor;

namespace GenericAuth
{
    public static class Authentication
    {
        public static AuthConfiguration Config { get; } = new AuthConfiguration();
        private static string Username { get; set; }
        public static SessionDictionary Session { get; } = new SessionDictionary();
        public static List<IAuthProvider> AuthProviders { get; set; } = new List<IAuthProvider>();

        public static TwoFactorCode GetTwoFactorCode(string secret)
        {
            var result = new TwoFactorCode();

            if (Username != null)
            {
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                var setupInfo = tfa.GenerateSetupCode(Config.AppName, Username, secret, 150, 150);

                var wr = WebRequest.Create(setupInfo.QrCodeSetupImageUrl).GetResponse().GetResponseStream();
                result.QrCode = Image.FromStream(wr);
                result.Secret = secret;
                result.ManualKey = setupInfo.ManualEntryKey;
            }
            else
            {
                throw new UnauthorizedAccessException("Cant do 2-Factor-Authorisation. User not authed!");
            }

            return result;
        }
        public static TwoFactorCode GetTwoFactorCode()
        {
            return GetTwoFactorCode(Guid.NewGuid().ToString());
        }

        public static event Action OnLogin;
        public static event Action OnLogout;

        public static bool Authenticate(string username, string password, IAuthProvider provider = null)
        {
            bool result = false;

            if (provider != null)
            {
                result = provider.Authenticate(username, password);
            }
            else
            {
                result = Config.DefaultAuthProvider.Authenticate(username, password);
            }
            if (result)
            {
                Username = username;
                Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(Username), new string[] {});
            }

            return result;
        }

        public static void Logout()
        {
            Username = null;
            Session.Clear();
            Thread.CurrentPrincipal = null;
            Authentication.RaiseOnLogout();
        }

        internal static void RaiseOnLogout()
        {
            var handler = OnLogout;
            if (handler != null) handler();
        }

        internal static void RaiseOnLogin()
        {
            var handler = OnLogin;
            if (handler != null) OnLogin();
        }
    }
}
﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GenericAuth.Core.UI
{
    public class LoginButton : PictureBox
    {
        public LoginButton()
        {
            Size = new Size(240, 25);
            SizeMode = PictureBoxSizeMode.Zoom;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            Cursor = Cursors.Arrow;
        }
    }
}
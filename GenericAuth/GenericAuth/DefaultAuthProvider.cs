﻿namespace GenericAuth
{
    public abstract class DefaultAuthProvider
    {
        public abstract bool Authenticate(string username, string passwort);
    }
}
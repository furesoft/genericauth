﻿using System.IO;

namespace GenericAuth
{
    public interface IAuthProvider
    {
        bool Authenticate();
        Stream GetLoginImage();
    }
}
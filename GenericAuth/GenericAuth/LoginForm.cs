﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GenericAuth.Core.UI;

namespace GenericAuth
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            morePanel.IsExpanded = false;

            InitializeButtons();
        }

        void InitializeButtons()
        {
            foreach (var authProvider in Authentication.AuthProviders)
            {
                var lb = new LoginButton();
                lb.Image = Image.FromStream(authProvider.GetLoginImage());
                lb.Click += (s, e) => authProvider.Authenticate();

                moreContentFlowPanel.Controls.Add(lb);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Authentication.Authenticate(usernameTextBox.Text, passwordTextBox.Text))
            {
                Hide();

                var code = Authentication.GetTwoFactorCode("Hello World");
                if (code.ShowDialog() == DialogResult.OK)
                {
                    Authentication.Session.Set("username", usernameTextBox.Text);

                    Authentication.RaiseOnLogin();
                }
                else
                {
                    MessageBox.Show("Code is not valid");
                }
            }
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GenericAuth
{
    public class SessionDictionary : Dictionary<String, object>
    {
        public object Get(string k)
        {
            if (ContainsKey(k))
            {
                return this[k];
            }
            return null;
        }

        public void Set(string k, object v)
        {
            if (ContainsKey(k))
            {
                this[k] = v;
            }
            else
            {
                Add(k, v);
            }
        }
    }
}
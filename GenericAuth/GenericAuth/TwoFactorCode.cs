using System.Drawing;
using GenericAuth.Core.TwoFactor;
using System.Windows.Forms;

namespace GenericAuth
{
    public class TwoFactorCode
    {
        public Image QrCode { get; set; }
        internal string Secret { get; set; }
        public string ManualKey { get; set; }

        public bool Validate(string key)
        {
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();

            return tfa.ValidateTwoFactorPIN(Secret, key);
        }

        public DialogResult ShowDialog()
        {
            return new TwoFactorDialog(this).ShowDialog();
        }
    }
}
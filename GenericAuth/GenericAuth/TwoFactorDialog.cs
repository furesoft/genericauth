﻿using System;
using System.Windows.Forms;

namespace GenericAuth
{
    public partial class TwoFactorDialog : Form
    {
        private readonly TwoFactorCode _code;

        public TwoFactorDialog(TwoFactorCode code)
        {
            _code = code;

            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (_code.Validate(keyTextBox.Text))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Abort;
            }
        }

        private void TwoFactorDialog_Load(object sender, EventArgs e)
        {
            qrPictureBox.Image = _code.QrCode;
        }
    }
}
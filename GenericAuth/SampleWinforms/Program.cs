﻿using System;
using System.Windows.Forms;
using GenericAuth;

namespace SampleWinforms
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Authentication.Config.AppName = "Sample";
            Authentication.Config.DefaultAuthProvider = new SampleAuthProvider();

            Authentication.OnLogin += () =>
            {
                MessageBox.Show("Logged in");

                Authentication.Logout();
            };
            Authentication.OnLogout += () =>
            {
                MessageBox.Show("You Are Now Logged Out");
                Application.Exit();
            };

            Application.Run(new LoginForm());
        }
    }
}
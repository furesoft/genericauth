using GenericAuth;

namespace SampleWinforms
{
    internal class SampleAuthProvider : DefaultAuthProvider
    {
        public override bool Authenticate(string username, string passwort)
        {
            return username == "admin" && passwort == "admin";
        }
    }
}